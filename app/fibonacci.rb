module Memorize
  @@memory = []

  def memorizate(method_name)

    private_method = "_#{method_name}_"
    alias_method private_method, method_name

    define_method method_name do |position|
      @@memory[position] ||= send(private_method,position) 
    end
  end
  
end

class Fibonacci
  extend Memorize
  def at_position(pos)
    return pos if pos < 2
    at_position(pos - 1) + at_position(pos - 2)
  end
  memorizate :at_position
end